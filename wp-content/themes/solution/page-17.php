﻿<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
<div id="forhomepageonly" class="slider-wrapper theme-default" style="margin-top:20px;padding-bottom:10px;">
    <?php if ( function_exists('show_nivo_slider') ) { show_nivo_slider(); } ?>
</div>
<div class="master-wrapper-main">
            

<div class="side-2">
	<?php
	genesis_after_content_sidebar_wrap();
   ?>    

</div>
<div class="center-2">
    
    
	<div class="page home-page">
		<div class="page-body">        
			
			
			<div class="product-grid home-page-product-grid">
				<div class="title">
					<strong>Liên hệ</strong>
				</div>
				<div class="contacts">
				<?php				
					if(have_posts()):while(have_posts()):the_post();
						the_content();
					endwhile;endif;wp_reset_query();
				?>
				</div>
			</div>	
		</div>
	</div><!--page home-->

    
</div>

        </div>
	
	<?php get_footer(); ?>