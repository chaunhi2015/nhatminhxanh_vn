<?php
/**
 * WARNING: This file is part of the core Genesis framework. DO NOT edit
 * this file under any circumstances. Please do all modifications
 * in the form of a child theme.
 */
genesis_doctype();

genesis_meta();
?>
<meta content="INDEX,FOLLOW" name="robots" />
<?php
wp_head(); // we need this for plugins

genesis_title();

?>
    
	<link href="<?php bloginfo('stylesheet_directory') ?>/style.css" rel="stylesheet" type="text/css" />
<!--<link href="<?php bloginfo('stylesheet_directory') ?>/css/responsive.css" rel="stylesheet" type="text/css" />-->
<link href="<?php bloginfo('stylesheet_directory') ?>/Content/jquery-ui-themes/smoothness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />


<script src="<?php bloginfo('stylesheet_directory') ?>/Scripts/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/Scripts/jquery.validate.unobtrusive.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/Scripts/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/Scripts/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/Scripts/public.common.js" type="text/javascript"></script>
<script src="<?php bloginfo('stylesheet_directory') ?>/Scripts/public.ajaxcart.js" type="text/javascript"></script>

    <!-- Custom Fonts -->
    <link href="<?php bloginfo('stylesheet_directory') ?>/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "52296926-f34e-48ca-a149-b6505aa6862c", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
</head>
<body>
<div class="master-wrapper-page">
    
    <div class="master-wrapper-content">

<div class="header">    
        <a href="<?php bloginfo("url");?>">
            <img title="" alt="Your store name" src="<?php bloginfo('stylesheet_directory') ?>/images/header.jpg">
        </a>    
</div>

        <div class="header-menu">
            
			<?php
				if ( genesis_get_option('nav_type') == 'nav-menu' && function_exists('wp_nav_menu') ) {
					
					$nav = wp_nav_menu(array(
						'theme_location' => 'primary',
						'container' => '',									
						'menu_class' => genesis_get_option('nav_superfish') ? 'menus top-menu' : 'top-menu',
						'echo' => 0
					));	
				} else {
					$nav = genesis_nav(array(
						'theme_location' => 'primary',
						'menu_class' => genesis_get_option('nav_superfish') ? 'nav superfish' : 'nav',
						'show_home' => genesis_get_option('nav_home'),
						'type' => genesis_get_option('nav_type'),
						'sort_column' => genesis_get_option('nav_pages_sort'),
						'orderby' => genesis_get_option('nav_categories_sort'),
						'depth' => genesis_get_option('nav_depth'),
						'exclude' => genesis_get_option('nav_exclude'),
						'include' => genesis_get_option('nav_include'),
						'echo' => false
					));
					
				}
				
				echo $nav;
			?>
			<div class="linkhome" style="display:none">
			<?php
				if($_SESSION['language'] == 'en_US'){
					echo '<a href="'.get_bloginfo("url").'">Home</a>';
				}else{
					echo '<a href="'.get_bloginfo("url").'">Trang chủ</a>';
				}
			?>
			</div>
			<div class="titleproduct" style="display:none">
			<?php
				if($_SESSION['language'] == 'en_US'){
					echo 'Category';
				}else{
					echo 'Sản phẩm';
				}
			?>
			</div>
			<div class="titlesupport" style="display:none">
			<?php
				if($_SESSION['language'] == 'en_US'){
					echo 'Support';
				}else{
					echo 'Tư Vấn Bán Hàng';
				}
			?>
			</div>
			<div class="titlestas" style="display:none">
			<?php
				if($_SESSION['language'] == 'en_US'){
					echo 'Statistical';			
				}else{
					echo 'Thống kê';
				}
			?>			
			</div>
			<?php
				if($_SESSION['language'] == 'en_US'){
			?>
				<script>
				jQuery(document).ready(function(){
					jQuery(".info-hotrokt").html('<p>Support 1</p><p><span class="h_phone">0965-611-001</span></p><p>Support 2</p><p><span class="h_phone">098-626-3225</span></p>');
				});
				</script>
			<?php } ?>
			<script type="text/javascript">
				jQuery('li', '.top-menu').on('mouseenter', function () {
					jQuery('a', jQuery(this)).first().addClass('hover');
					if (!jQuery(this).parent().hasClass('top-menu')) {
						var width = jQuery(this).innerWidth();
						jQuery('.sub-menu', jQuery(this)).first().css('left', width + 15);
					}
					jQuery('.sub-menu', jQuery(this)).first().addClass('active');
					jQuery('.top-menu-triangle', jQuery(this)).addClass('active');
				});

				jQuery('li', '.top-menu').on('mouseleave', function () {
					jQuery('a', jQuery(this)).first().removeClass('hover');
					jQuery('.sub-menu', jQuery(this)).first().removeClass('active');
					jQuery('.top-menu-triangle', jQuery(this)).removeClass('active');
				});
				var linkhome = jQuery(".linkhome").html();
				var titleproduct = jQuery(".titleproduct").text();
				var titlesupport = jQuery(".titlesupport").text();
				var titlestas = jQuery(".titlestas").text();
				jQuery("li.menu-item-type-custom").html(linkhome);
				jQuery(document).ready(function(){
					jQuery("#nav_menu-2 h4.widgettitle").html(titleproduct);
					jQuery("#text-2 h4.widgettitle").html(titlesupport);
					jQuery("#text-3 h4.widgettitle").html(titlestas);
				});							
			</script>
			<?php if ( function_exists( 'mltlngg_display_switcher' ) ) mltlngg_display_switcher(); ?>
    

        </div><!---header menu-->
