<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
<div id="forhomepageonly" class="slider-wrapper theme-default" style="margin-top:20px;padding-bottom:10px;">
    <?php if ( function_exists('show_nivo_slider') ) { show_nivo_slider(); } ?>
</div>
<div class="master-wrapper-main">
            

<div class="side-2">
	<?php
	genesis_after_content_sidebar_wrap();
   ?>    

</div>
<div class="center-2">
    
    
	<div class="page home-page">
		<div class="page-body">        
			
			<?php if(have_posts()):while(have_posts()):the_post();?>
			<div class="product-grid home-page-product-grid">
				<div class="title">
					<strong><?php the_title();?></strong>
				</div>
				<div class="content">
					<?php the_content();?>
					<p>Chia sẻ bài viết:</p>
					<p>
					<span class='st_sharethis_hcount' displayText='ShareThis'></span>
<span class='st_facebook_hcount' displayText='Facebook'></span>
<span class='st_twitter_hcount' displayText='Tweet'></span>
<span class='st_googleplus_hcount' displayText='Google +'></span>
<span class='st_email_hcount' displayText='Email'></span>
					</p>
				</div>        
			</div>	
			<?php endwhile;endif;wp_reset_query();?>
		</div>
	</div><!--page home-->

    
</div>

        </div>
	
	<?php get_footer(); ?>