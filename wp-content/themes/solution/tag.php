<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	<div class="master-wrapper-main">
            <div class="master-wrapper-content">
                


<div class="center-2">
    <div class="breadcrumb">
        <?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
    </div>
	<div class="page category-page">
		<div class="page-title">
			<h1><?php single_cat_title();?></h1>
		</div>
		<div class="page-body">
		
				
			<div style="border-bottom:1px solid #ebebeb;margin-bottom:20px"></div>
			<div class="item-selectors">					
					<div class="item-sorting">
						<span>Sort by</span>
						<select id="item-orderby" name="item-orderby" onchange="setLocation(this.value);">
							<option selected="selected" value="<?php echo $current_url."?created=desc" ?>">Position</option>
							<option value="<?php echo $current_url."?title=asc" ?>">Name: A to Z</option>
							<option value="<?php echo $current_url."?title=desc" ?>">Name: Z to A</option>
							<option value="<?php echo $current_url."?price=asc" ?>">Price: Low to High</option>
							<option value="<?php echo $current_url."?price=desc" ?>">Price: High to Low</option>
							<option value="<?php echo $current_url."?created=desc" ?>">Created on</option>
						</select>
					</div>
					<div class="item-page-size">
						<span>Display</span>
						<select id="item-pagesize" name="item-pagesize" onchange="setLocation(this.value);">
							<option selected="selected" value="<?php echo $current_url."?pagesize=8" ?>">8</option>
							<option value="<?php echo $current_url."?pagesize=12" ?>">12</option>
						</select>
						<span>per page</span>
					</div>
			</div>
			
			
			
				<div class="tour-list">
					<?php
					$name = isset($_GET["title"]) ? $_GET["title"]:"";
					$price = isset($_GET["price"]) ? $_GET["price"]:"";
					$created = isset($_GET["created"]) ? $_GET["created"]:"";
					$pagesize = isset($_GET["pagesize"]) ? $_GET["pagesize"]:"8";
					
					$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
					if(isset($_GET['title'])){
						$arg = array('posts_per_page' => $pagesize,'paged'=> $paged,'cat'=>$thiscat,'orderby' => 'title', 'order'=> $name);						
						query_posts($arg);
					}else if(isset($_GET["price"])){
						$arg = array('posts_per_page' => $pagesize,'paged'=> $paged,'cat'=>$thiscat,'orderby' => array( 'meta_value_num' => $price), 'meta_key'=>'wpcf-price-to-compare');						
						query_posts($arg);
					}else{
						$arg = array('posts_per_page' => $pagesize,'paged'=> $paged,'cat'=>$thiscat,'order'=>'DESC');						
						query_posts($arg);
					}					
						if(have_posts()):while(have_posts()):the_post();
					?>
						<div class="item-box">                
							<div class="tour-item" data-tourid="1132">
								<div class="picture">
									<a href="<?php the_permalink();?>" title="<?php the_title();?>">
										<?php the_post_thumbnail('full');?>
									</a>
								</div>
								<div class="details">
									<h2 class="tour-title">
										<a href="<?php the_permalink();?>"><?php the_title();?></a>
									</h2>
									<?php 		
										$duration = '';
										$data = get_post_meta( get_the_ID(), 'wpcf-duration', false );								
										foreach($data as $val){															
											$duration = $val;
										} 
									?>
										<?php if($duration){ ?>
											<div class="duration">									
												<span class="label">Duration</span>: <span class="value" itemprop="duration"><?php echo $duration;?></span>
											</div>
										<?php } ?>
										<?php 		
											$destinations = '';
											$data = get_post_meta( get_the_ID(), 'wpcf-destinations', false );								
											foreach($data as $val){															
												$destinations = $val;
											} 
										?>
										<?php if($destinations){ ?>
											<div class="destinations">
												<span class="label">Destinations</span>: <span class="value" itemprop="destiantions"><?php echo $destinations;?></span>
											</div>
										<?php } ?>
									<div class="description">
										<?php echo get_the_excerpt();?>
									</div>
									<div class="add-info">
										<?php 		
											$price_vnd = '';
											$price_usd = '';
											$data = get_post_meta( get_the_ID(), 'wpcf-price-vnd', false );								
											foreach($data as $val){															
												$price_vnd = $val;
											}
											$data = get_post_meta( get_the_ID(), 'wpcf-price-usd', false );								
											foreach($data as $val){															
												$price_usd = $val;
											}
										?>
										<?php if($price_vnd){ ?>
											<div class="prices">
												<span class="pricerange">From:</span>
												<span class="price actual-price"><?php echo $price_vnd;?> VND</span> <span class="pricerange">equals</span> <span class="price actual-price"><?php echo $price_usd;?> USD</span>
											</div>
										<?php }else{
										?>
											<div class="prices">
												<span class="pricerange">From:</span>
												<span class="price actual-price"></span> <span class="pricerange">equals</span> <span class="price actual-price">Call for pricing</span>
											</div>
										<?php
										} ?>
										<div class="buttons">
											<input type="button" value="Details" class="detail-button1" onclick="setLocation('<?php the_permalink();?>')" />									
										</div>
										
									</div>
								</div>
							</div>
						</div>
					<?php
							endwhile;endif;wp_reset_query();
						?>
				</div>       
			<div class="pager">
				
			</div>
			
		</div>
	</div>

    
</div>
<div class="side-2">

            <div>
<div id="TA_selfserveprop95" class="TA_selfserveprop"><div id="CDSWIDSSP" class="widSSP widSSPnarrow" style="width: 240px;"> <div class="widSSPData" style="border: 1px solid #589442;"> <div class="widSSPBranding"> <dl> <dt> <a target="_blank" href="http://www.tripadvisor.com/"><img src="http://www.tripadvisor.com/img/cdsi/img2/branding/150_logo-11900-2.png" alt="TripAdvisor"></a> </dt> <dt class="widSSPTagline">Book your best trip, every trip</dt> </dl> </div><!--/ cdsBranding--> <div class="widSSPComponent"> <div class="widSSPSummary"> <dl> <a target="_blank" href="http://www.tripadvisor.com/Attraction_Review-g293924-d2234933-Reviews-Go_Asia_Travel_Day_Tours-Hanoi.html" onclick="ta.cds.handleTALink(11900,this);return true;" rel="nofollow"> <dt class="widSSPH18">Go Asia Travel - Day Tours</dt> </a> </dl> </div><!--/ cdsSummary--> </div><!--/ cdsComponent--> <div class="widSSPComponent widSSPOptional"> <div class="widSSPTrvlRtng"> <dl> <dt class="widSSPH11">TripAdvisor Traveler Rating</dt> <dd> <div class="widSSPOverall"> <img src="http://static.tacdn.com/img2/ratings/traveler/s4.5.gif" alt="4.5 of 5 stars" class="rsImg"> <div>Based on <b>309</b> traveler reviews</div> </div><!--/ overall --> </dd> </dl> </div> </div><!--/ cdsComponent --> <div class="widSSPWrap widSSPOptional"> <div class="widSSPInformation"> <div class="widSSPWrap"> <div class="widSSPPopIdx widSSPSingle"> <b>TripAdvisor Ranking</b> <span class="widSSPPopIdxData"> <span class="widSSPPopIdxData widSSPPopIdxNumbers"> <sup>#</sup>8 of 146 </span> Boat Tours &amp; Water Sports in Hanoi </span> </div><!--/ popIdx--> </div><!--/ cdsWrap--> </div><!--/ cdsInformation--> </div><!--/ cdsWrap--> <div class="widSSPComponent widSSPOptional"> <dl class="widSSPReviews"> <dt class="widSSPH11">Most Recent Traveler Reviews</dt> <dd class="widSSPOneReview"> <ul class="widSSPBullet"> <li> <span class="widSSPDate">Dec 10, 2015:</span> <span class="widSSPQuote">“Everything promised was delivered on 11 day...”</span> </li> <li> <span class="widSSPDate">Dec 10, 2015:</span> <span class="widSSPQuote">“Day trip to Hanoi”</span> </li> <li> <span class="widSSPDate">Dec 8, 2015:</span> <span class="widSSPQuote">“14 Day Best of Vietnam &amp; Cambodia...”</span> </li> <li> <span class="widSSPDate">Dec 6, 2015:</span> <span class="widSSPQuote">“Excelent tour guide”</span> </li> <li> <span class="widSSPDate">Dec 5, 2015:</span> <span class="widSSPQuote">“Great Guide and TRip”</span> </li> </ul><!--/ bullet--> </dd><!--/ hReview--> </dl> </div> <div class="widSSPAll"> <ul class="widSSPReadReview"> <li><a href="http://www.tripadvisor.com/Attraction_Review-g293924-d2234933-Reviews-Go_Asia_Travel_Day_Tours-Hanoi.html" id="allreviews" onclick="ta.cds.handleTALink(11900,this);window.open(this.href, 'newTAWindow', 'toolbar=1,resizable=1,menubar=1,location=1,status=1,scrollbars=1,width=800,height=600'); return false" rel="nofollow">Read reviews</a></li> </ul> <ul class="widSSPWriteReview"> <li><a href="https://www.tripadvisor.com/UserReview-g293924-d2234933-Go_Asia_Travel_Day_Tours-Hanoi.html" id="writereview" onclick="ta.cds.handleTALink(11900,this);window.open(this.href, 'newTAWindow', 'toolbar=1,resizable=1,menubar=1,location=1,status=1,scrollbars=1,width=800,height=600'); return false" rel="nofollow">Write a review</a></li> </ul> </div><!--/ cdsAll--> <div class="widSSPLegal">© 2015 TripAdvisor LLC</div><!--/ cdsLegal--> </div><!--/ cdsData--> </div><!--/ CDSPOP.cdsBx--> </div>

<script src="http://www.jscache.com/wejs?wtype=selfserveprop&amp;uniq=95&amp;locationId=2234933&amp;lang=en_US&amp;rating=true&amp;nreviews=5&amp;writereviewlink=true&amp;popIdx=true&amp;iswide=false&amp;border=true&amp;langversion=2"></script><script src="http://www.tripadvisor.com/WidgetEmbed-selfserveprop?border=true&amp;popIdx=true&amp;iswide=false&amp;locationId=2234933&amp;uniq=95&amp;rating=true&amp;lang=en_US&amp;nreviews=5&amp;writereviewlink=true&amp;langversion=2"></script>                
            </div>



   <?php
	genesis_after_content_sidebar_wrap();
   ?>            

</div>
            </div>
        
    </div>	
	<?php get_footer(); ?>