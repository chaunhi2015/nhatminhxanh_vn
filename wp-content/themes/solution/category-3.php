<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
<div id="forhomepageonly" class="slider-wrapper theme-default" style="margin-top:20px;padding-bottom:10px;">
    <?php if ( function_exists('show_nivo_slider') ) { show_nivo_slider(); } ?>
</div>
<div class="master-wrapper-main">
            

<div class="side-2">
	<?php
	genesis_after_content_sidebar_wrap();
   ?>    

</div>
<div class="center-2">
    
    
	<div class="page home-page">
		<div class="page-body">        
			
			
			<div class="product-grid home-page-product-grid">
				<div class="title">
					<strong><?php single_cat_title();?></strong>
				</div>
				<?php
					if(have_posts()):while(have_posts()):the_post();
				?>
				<div class="tin_tuc">
					<div class="row">
						<div class="large-4 column">
							<a class="th" href="<?php the_permalink();?>" title="<?php the_title();?>">
								<?php the_post_thumbnail('thumbnail');?>
							</a>
						</div>
						<div class="large-8 column">
							<h2>
							<a href="<?php the_permalink();?>"><?php the_title();?></a>
							</h2>
							<?php the_excerpt();?>
						</div>
						<div class="more">
						<a href="<?php the_permalink();?>">Đọc tiếp</a>
						</div>
					</div>
				</div>  
				<?php
					endwhile;endif;wp_reset_query();
				?>
			</div>	
		</div>
	</div><!--page home-->

    
</div>

        </div>
	
	<?php get_footer(); ?>