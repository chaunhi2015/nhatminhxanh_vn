<?php get_header(); ?>

<?php genesis_before_content_sidebar_wrap(); ?>

	<?php genesis_before_content(); ?> 
	
<div id="forhomepageonly" class="slider-wrapper theme-default" style="margin-top:20px;padding-bottom:10px;">
    <?php if ( function_exists('show_nivo_slider') ) { show_nivo_slider(); } ?>
</div>
<div class="master-wrapper-main">
            

<div class="side-2">
	<?php
	genesis_after_content_sidebar_wrap();
   ?>    

</div>
<div class="center-2">
    
    
	<div class="page home-page">
		<div class="page-body">        
			
			
			<div class="product-grid home-page-product-grid">
				<div class="title">
					<strong><?php single_cat_title();?></strong>
				</div>
				<?php if(have_posts()):while(have_posts()):the_post();?>
					<div class="item-box">
						
						<div class="product-item" data-productid="1">
							<div class="picture">
								<a href="<?php the_permalink();?>" title="<?php the_title();?>">
									<img alt="<?php the_title();?>" src="<?php the_post_thumbnail("large");?>" title="<?php the_title();?>" />
								</a>
							</div>
							<div class="details">
								<h2 class="product-title">
									<a href="<?php the_permalink();?>"><?php the_title();?></a>
								</h2>
								<div class="txt">
										<span class="price">
										<?php										
										if($_SESSION['language'] == 'en_US'){
											echo "Contact";
										}else{
											echo "Liên hệ";
										}
										?>
										</span>
										<a href="<?php the_permalink();?>"><span class="detailx">
										<?php 
										if($_SESSION['language'] == 'en_US'){
											echo "Detail";
										}else{
											echo "Chi tiết";
										}
										?>
										</span></a>
									</div>
							</div>
						</div>

					</div>
				<?php endwhile;endif;wp_reset_query();?>   
			</div>	
		</div>
	</div><!--page home-->

    
</div>

        </div>
	
	<?php get_footer(); ?>