	<div class="footer">
			<div class="xf-topwrap logo-d">
				<div class="fsize">            	
					<?php
						query_posts("page_id=247");
						if(have_posts()):while(have_posts()):the_post();
							the_content();
						endwhile;endif;wp_reset_query();
					?>
					<script type="text/javascript">// <![CDATA[
						jQuery(document).ready(function($) {
						 
						  $(".ul-logo").owlCarousel({
						 
							  navigation : true, // Show next and prev buttons
							  slideSpeed : 300,
							  paginationSpeed : 400,
							  responsiveClass:true,
							responsive:{
								0:{
									items:1,
									nav:true
								},
								600:{
									items:3,
									nav:false
								},
								1024:{
									items:5,
									nav:true,
									loop:false
								}
							}
						  });
						 
						});
					// ]]></script>
				</div>
			</div>
			<div class="xf-middlewrap">
				<div class="fsize">
					<div class="f-box-row">						
						<h4><?php echo get_cat_name(3);?></h4>
						<ul>
							<?php
								query_posts("cat=3&posts_per_page=4");
								if(have_posts()):while(have_posts()):the_post();
							?>
							<li><a href="<?php the_permalink();?>"><i class="icons fa fa-caret-right"></i><?php the_title();?></a></li>							
							<?php
								endwhile;endif;wp_reset_query();
							?>
						</ul>
					</div>
					<div class="f-box-row">

						<h4><?php echo get_cat_name(17);?></h4>
						<ul>
							<?php
								query_posts("cat=17&posts_per_page=4");
								if(have_posts()):while(have_posts()):the_post();
							?>
							<li><a href="<?php the_permalink();?>"><i class="icons fa fa-caret-right"></i><?php the_title();?></a></li>							
							<?php
								endwhile;endif;wp_reset_query();
							?>
						</ul>
					</div>
					<div class="f-box-row">
						<h4><?php echo get_cat_name(4);?></h4>
						<ul>
							<?php
								query_posts("cat=4&posts_per_page=4");
								if(have_posts()):while(have_posts()):the_post();
							?>
							<li><a href="<?php the_permalink();?>"><i class="icons fa fa-caret-right"></i><?php the_title();?></a></li>							
							<?php
								endwhile;endif;wp_reset_query();
							?>
						</ul>
					</div>
					<div class="right-contact">
						<h4>
						<?php										
						if($_SESSION['language'] == 'en_US'){
							echo "CONTACT";
						}else{
							echo "LIÊN HỆ";
						}
						?>
						</h4>
						<ul>
							<li><i class="icons fa fa-map-marker"></i>Số 12, ngõ 76/5/6, Nguyễn Chí Thanh, Láng thượng, Đống đa, Hà Nội</li>
							<!--<li><i class="icons fa fa-phone"></i><strong>08. 6259 1817</strong></li>
							<li><i class="icons fa-fax fa"></i><strong>08. 6259 1815</strong></li>-->
							<li><i class="icons fa-envelope fa"></i><a href="mailto:autohoan@gmail.com">autohoan@gmail.com</a></li>
						   <li class="social-media">
								FOLLOW US
								<ul>
									<li class="social-googleplus tooltip-hover" title="Google+"><a href="#" target="_blank"></a></li>
									<li class="social-facebook tooltip-hover" title="Facebook"><a href="#" target="_blank"></a></li>
									<li class="social-youtube tooltip-hover" title="Youtube"><a href="#" target="_blank"></a></li>
								</ul>
						   </li>
						</ul>
					</div>
				</div>
			</div>
			
		</div><!--end of footer-->
    </div>
    	
</div>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/owl-carousel/owl.carousel.css" media="all" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/owl-carousel/owl.theme.css" media="all" />
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/owl-carousel/owl.carousel.js"></script>
<?php
	wp_footer(); // we need this for plugins
	genesis_after();
?>
<script src="http://uhchat.net/code.php?f=0ee923"></script>
</body>
</html>